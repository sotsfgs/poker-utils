# compile the project
compile:
    cmake -B build -G "Ninja" .
    cd build && cmake --build .

test: compile
    cd build && ninja test
