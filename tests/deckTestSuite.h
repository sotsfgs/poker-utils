#include "../src/deck.hpp"
#include <cxxtest/TestSuite.h>

using namespace poker;

class DeckTestSuite : public CxxTest::TestSuite {
public:
  void testDeckOrdered(void) {
    auto result = getDeck();

    TS_ASSERT_EQUALS(result.size(), 52);
    TS_ASSERT_EQUALS(result[0], "2s");
  }

  void testDeckShuffled(void) {
    auto deckA = getShuffledDeck();
    auto deckB = getShuffledDeck();

    TS_ASSERT_DIFFERS(deckA[0], deckB[0]);
    TS_ASSERT_DIFFERS(deckA[1], deckB[1]);
    TS_ASSERT_DIFFERS(deckA[2], deckB[2]);
    TS_ASSERT_DIFFERS(deckA[3], deckB[3]);
  }
};
