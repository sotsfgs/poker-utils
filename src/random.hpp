#ifndef RANDOM_H_
#define RANDOM_H_
#include <bitset>
#include <iostream>
#include <random>
#include <sstream>
#include <string>

namespace poker {
std::bitset<226> getRandomSHA256();
}
#endif // RANDOM_H_
