#include "hand-evaluation.hpp"
#include "../ompeval/omp/EquityCalculator.h"
#include "../ompeval/omp/HandEvaluator.h"
#include "cards.hpp"
#include <algorithm>
#include <cmath>
#include <string>
#include <variant>
#include <vector>

namespace poker {
const int fixhandval = 4096;

std::string getHandName(int handValue) {
  const int rank = getHandRank(handValue);
  std::string handName = "";

  switch (rank) {
  case 1:
    handName = "High card";
    break;
  case 2:
    handName = "Pair";
    break;
  case 3:
    handName = "Two pair";
    break;
  case 4:
    handName = "Three of a kind";
    break;
  case 5:
    handName = "Straight";
    break;
  case 6:
    handName = "Flush";
    break;
  case 7:
    handName = "Full house";
    break;
  case 8:
    handName = "Four of a kind";
    break;
  case 9:
    handName = "Straight flush";
    break;
  case 10:
    handName = "Royal flush";
    break;
  }

  return handName;
}

Error validateCards(const std::vector<std::string> cards) {
  if (size(cards) > 52) {
    return Error{"amount of cards is above a standard deck of 52 cards"};
  }

  std::vector<std::string> copy;

  copy = cards;

  std::sort(copy.begin(), copy.end());

  int matches;
  std::string current_match;
  for (const auto &card : copy) {
    if (card == current_match) {
      matches++;
      if (matches >= 2) {
        return Error{"you cannot have " + current_match + " twice or more"};
      }
    }
    current_match = card;
    matches = 1;
  }

  return Error{};
}

int getHandRank(int handValue) {
  // check we have a Royal flush
  if (handValue == 36874) {
    return 10;
  }
  return handValue / fixhandval;
}

bool compareHandsRanking(const HandRankEvaluation &a,
                         const HandRankEvaluation &b) {
  return a.Value > b.Value;
}

bool compareHandWin(const WinTieEvaluation &a, const WinTieEvaluation &b) {
  return a.Win > b.Win;
}

SingleHandRankEvaluationResult singleHandRankEvaluation(const Cards cards) {
  Error err = validateCards(cards);
  if (err.message != "") {
    return err;
  }

  omp::HandEvaluator eval;
  omp::Hand hand = omp::Hand::empty();

  for (const auto &card : cards) {
    auto result = getCardIndex(card);
    if (std::holds_alternative<Error>(result)) {
      return std::get<Error>(result); // Propagate error
    }
    hand += omp::Hand(std::get<int>(result));
  }

  return eval.evaluate(hand);
}

HandsRankingEvaluationResult
handsRankingEvaluation(Cards board, std::vector<PlayerHand> players) {
  std::vector<HandRankEvaluation> results;

  auto hand_value = singleHandRankEvaluation(board);
  if (std::holds_alternative<Error>(hand_value)) {
    return std::get<Error>(hand_value);
  }

  results.push_back(HandRankEvaluation{"table", std::get<int>(hand_value)});

  // process each player hand
  for (const auto &player : players) {
    // clone the table cards so the original vector remains the same
    // after merging the player's hand
    std::vector<std::string> all_cards(board);
    all_cards.insert(all_cards.end(), player.Cards.begin(), player.Cards.end());

    auto hand_value = singleHandRankEvaluation(all_cards);
    if (std::holds_alternative<Error>(hand_value)) {
      return std::get<Error>(hand_value);
    }

    results.push_back(
        HandRankEvaluation{player.PlayerID, std::get<int>(hand_value)});
  }

  // sort all the players from best to worst hand
  std::sort(results.begin(), results.end(), compareHandsRanking);

  return results;
}

WinTieCalculationResult
calculateWinTieFromEquities(std::vector<HandEquityEvaluation> equities,
                            int communityCards) {
  std::vector<WinTieEvaluation> results;

  const double EPSILON = 1e-4;
  bool isTie = true;
  int equal_values = equities.size();
  double last_value = equities.at(0).Value;

  bool hasClearWinner = false;
  for (const auto &equity : equities) {
    if (std::fabs(equity.Value - 1.0) < EPSILON) {
      hasClearWinner = true;
      break;
    }
  }

  for (const auto &equity : equities) {
    if (last_value != equity.Value) {
      last_value = equity.Value;
      equal_values--;
    }
  }

  if (equal_values < equities.size()) {
    isTie = false;
  }

  for (const auto &equity : equities) {
    WinTieEvaluation result{equity.PlayerID};

    if (hasClearWinner) {
      if (std::fabs(equity.Value - 1.0) < EPSILON) {
        result.Win = 1.0;
        result.Tie = 0.0;
      } else {
        result.Win = 0.0;
        result.Tie = 0.0;
      }
    } else if (communityCards == 5 && isTie) {
      result.Win = 0.0;
      result.Tie = 1.0;
    } else {
      result.Win = equity.Value;
      result.Tie = 0.0;
    }

    results.push_back(result);
  }

  std::sort(results.begin(), results.end(), compareHandWin);

  return results;
}

CalculateEquitiesResult calculateEquities(Cards board, Cards muck,
                                          std::vector<PlayerHand> players) {
  Cards all_cards = board;
  all_cards.insert(all_cards.end(), muck.begin(), muck.end());

  for (const auto &player : players) {
    all_cards.insert(all_cards.end(), player.Cards.begin(), player.Cards.end());
  }

  Error err = validateCards(all_cards);
  if (err.message != "") {
    return err;
  }

  // CardRange only accepts strings, not array of strings
  std::string boardStr = "";
  for (const auto &card : board) {
    boardStr += card;
  }
  uint64_t boardCardMask = omp::CardRange::getCardMask(boardStr);

  // Convert dead cards to a mask
  std::string deadStr = "";
  for (const auto &card : muck) {
    deadStr += card;
  }
  uint64_t dead = omp::CardRange::getCardMask(deadStr);

  std::vector<omp::CardRange> _playerHands;
  for (const auto &player : players) {
    // CardRange only accepts strings, not array of strings
    std::string player_hand = "";
    for (const auto &card : player.Cards) {
      player_hand += card;
    }
    _playerHands.push_back(omp::CardRange(player_hand));
  }

  omp::EquityCalculator eq;

  double stdErrMargin = 2e-5;
  double updateInterval = 0.25;
  auto callback = [&eq](const omp::EquityCalculator::Results &results) {
    if (results.time > 1) // Stop after 1 second
      eq.stop();
  };
  // Initialize equity calculator
  eq.start(_playerHands, boardCardMask, dead, true, stdErrMargin, callback,
           updateInterval);
  eq.wait();
  auto r = eq.getResults();

  // Extract equities and return
  std::vector<HandEquityEvaluation> results;
  for (size_t i = 0; i < players.size(); i++) {
    results.push_back(HandEquityEvaluation{players[i].PlayerID, r.equity[i]});
  }

  return results;
}
} // namespace poker
