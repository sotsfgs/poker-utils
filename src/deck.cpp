#include "deck.hpp"
#include "random.hpp"
#include <algorithm>
#include <random>
#include <string>
#include <vector>

namespace poker {
std::vector<std::string> ranks = {"2", "3", "4", "5", "6", "7", "8",
                                  "9", "T", "J", "Q", "K", "A"};
std::vector<std::string> suits = {"s", "h", "c", "d"};

std::vector<std::string> getDeck() {
  std::vector<std::string> deck = {};

  for (const auto &suit : suits) {
    for (const auto &rank : ranks) {
      deck.push_back(rank + suit);
    }
  }

  return deck;
}

std::vector<std::string> getShuffledDeck() {
  auto ordered_deck = getDeck();
  auto bitset_seed = getRandomSHA256();

  std::vector<unsigned long> seed_data;
  const size_t num_ulong =
      (bitset_seed.size() + sizeof(unsigned long) * 8 - 1) /
      (sizeof(unsigned long) * 8);
  seed_data.reserve(num_ulong);

  for (size_t i = 0; i < num_ulong; ++i) {
    unsigned long chunk = 0;
    for (size_t j = 0; j < sizeof(unsigned long) * 8; ++j) {
      size_t bit_index = i * sizeof(unsigned long) * 8 + j;
      if (bit_index < bitset_seed.size()) {
        chunk |= (bitset_seed[bit_index] << j);
      }
    }
    seed_data.push_back(chunk);
  }

  std::seed_seq seed(seed_data.begin(), seed_data.end());
  std::mt19937 gen(seed);

  std::shuffle(ordered_deck.begin(), ordered_deck.end(), gen);

  return ordered_deck;
}
} // namespace poker
