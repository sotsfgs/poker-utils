#ifndef DECK_H_
#define DECK_H_

#include <string>
#include <vector>

namespace poker {
std::vector<std::string> getDeck();

std::vector<std::string> getShuffledDeck();
} // namespace poker
#endif // DECK_H_
