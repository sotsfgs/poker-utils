#include "random.hpp"

namespace poker {
std::bitset<226> getRandomSHA256() {
  std::bitset<226> bits;
  std::random_device rd;

  for (size_t i = 0; i < 226; ++i) {
    bits[i] = rd() & 1;
  }

  return bits;
}
} // namespace poker
