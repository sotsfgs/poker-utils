#ifndef CARDS_H_
#define CARDS_H_

#include <string>
#include <variant>

namespace poker {
struct Error {
  std::string message;
};

using CardIndexResult = std::variant<int, Error>;

CardIndexResult getCardIndex(const std::string &card);
} // namespace poker
#endif // CARDS_H_
