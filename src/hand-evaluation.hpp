#ifndef HAND_EVALUATION_
#define HAND_EVALUATION_

#include "../ompeval/omp/HandEvaluator.h"
#include "cards.hpp"
#include <algorithm>
#include <string>
#include <unordered_map>
#include <variant>
#include <vector>

namespace poker {
struct PlayerHand {
  std::string PlayerID;
  std::vector<std::string> Cards;
};

struct WinTieEvaluation {
  std::string PlayerID;
  double Win;
  double Tie;
};

struct HandRankEvaluation {
  std::string PlayerID;
  int Value;
};

struct HandEquityEvaluation {
  std::string PlayerID;
  double Value;
};

using HandsRankingEvaluations = std::vector<HandRankEvaluation>;

using HandsRankingEvaluationResult =
    std::variant<HandsRankingEvaluations, Error>;

using SingleHandRankEvaluationResult = std::variant<int, Error>;

using CalculateEquitiesResult =
    std::variant<std::vector<HandEquityEvaluation>, Error>;

using WinTieCalculationResult = std::vector<WinTieEvaluation>;

using Cards = std::vector<std::string>;

// ** Functions

Error validateCards(const std::vector<std::string> cards);

int getHandRank(int handValue);

std::string getHandName(int handValue);

SingleHandRankEvaluationResult
singleHandRankEvaluation(const std::vector<std::string> cards);

HandsRankingEvaluationResult
handsRankingEvaluation(Cards board, std::vector<PlayerHand> players);

CalculateEquitiesResult calculateEquities(Cards board, Cards muck,
                                          std::vector<PlayerHand> players);

WinTieCalculationResult
calculateWinTieFromEquities(std::vector<HandEquityEvaluation> equities,
                            int communityCards);
} // namespace poker
#endif // HAND_EVALUATION_
