#include "cards.hpp"
#include <string>
#include <unordered_map>
#include <variant>

namespace poker {
std::unordered_map<std::string, int> rankMap = {
    {"2", 0},  {"3", 1}, {"4", 2}, {"5", 3},  {"6", 4},  {"7", 5},
    {"8", 6},  {"9", 7}, {"T", 8}, {"J", 9},  {"Q", 10}, {"K", 11},
    {"A", 12}, {"t", 8}, {"j", 9}, {"q", 10}, {"k", 11}, {"a", 12}};

std::unordered_map<std::string, int> suitMap = {
    {"s", 0}, {"h", 1}, {"c", 2}, {"d", 3}};

CardIndexResult getCardIndex(const std::string &card) {
  if (card.length() < 2) {
    return Error{"Invalid card length: size " + std::to_string(card.length()) +
                 " ('" + card + "')"};
  }
  std::string rankChar = card.substr(0, 1);
  std::string suitChar = card.substr(1, 1);

  auto rankIt = rankMap.find(rankChar);
  auto suitIt = suitMap.find(suitChar);

  if (rankIt == rankMap.end() || suitIt == suitMap.end()) {
    return Error{"Invalid card rank or suit for '" + card + "'"};
  }

  int rank = rankIt->second;
  int suit = suitIt->second;

  return 4 * rank + suit;
}
} // namespace poker
